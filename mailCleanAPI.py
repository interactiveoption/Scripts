#! usr/bin/python
import requests
import urllib
import json

TESTURL="http://mail-api.com/V1?Key=KATu3usPecEfexUs&Email="
#EMAIL = urllib.quote_plus('test@test.com')

#r = requests.get('http://mail-api.com/V1?Key=KATu3usPecEfexUs&Email=test@test.com')
#r2 = requests.get('http://mail-api.com/V1?Key=KATu3usPecEfexUs&Email=alan@interactiveoption.com')
whiteL=open('./APIClean_whiteList','w+')
grayL=open('./APIClean_grayList','w+')
blackL=open('./APIClean_blackList','w+')

sourceF = open('source.txt', 'r')

for line in sourceF:
	EMAIL = urllib.quote_plus(line.rstrip())
	#EMAIL=line
	req = 'http://mail-api.com/V1?Key=KATu3usPecEfexUs&Email='+EMAIL
	r = requests.get(req)
	r_parsed=json.loads(r.text)
	r_status=r_parsed['body'][0]['Status']
	r_email=r_parsed['body'][0]['Email']
	if r_status=="Failed":
		print >>blackL, r_email
	elif r_status=="OK":
		print >>whiteL, r_email
	else:
		print >>grayL, r_email
