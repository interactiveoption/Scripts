#!/bin/bash

### VARIABLES ### \
SERVER=$(hostname)
MYSQLCREDS="-u root -pa14ld4iGYk"
ACCEPTABLEDELAY=500
QUERY='select * from silverpopExport where date_format(lastUpdate,"%Y%m%d")>=20150723'
dFULL=`date +%Y%m%d`
TARGETFILE="/tmp/exportLeads_${dFULL}.csv"
FTPPASS='$io745POP8734$'
LASTEXPORTname=`ls -tl /tmp/exportLeads_* |head -1 |cut -d "_" -f 2`
LASTEXPORT=`basename ${LASTEXPORTname} .csv`
echo "LASTEXPORTname:${LASTEXPORTname}"
echo "LASTEXPORT:${LASTEXPORT}"
re='^[0-9]+$'
if ! [[ ${LASTEXPORT} =~ $re ]] ; then
   echo "error: Not a number" >&2;
   LASTEXPORT=$((${dFULL}-1))
   echo "LASTEXPORT:${LASTEXPORT}"
fi
mysql ${MYSQLCREDS} << EOF
use iobi;
select Uid, email, Alias, regTime, FirstDepositDate, Country, Language, agentEmail, Site from silverpopExport where date_format(lastUpdate,"%Y%m%d")>=${LASTEXPORT}
INTO OUTFILE '${TARGETFILE}'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n' ;
EOF

sshpass -p "${FTPPASS}" scp ${TARGETFILE} alain.p@interactiveoption.com@transfer3.silverpop.com:/upload/s2.csv
