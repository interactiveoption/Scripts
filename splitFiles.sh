#! /bin/bash

####################################################################
####################################################################
#  Grep & process emails from files
#                                                                ###
####################################################################
SCRIPTNAME="SplitFiles"
####################################################################
#WHERE="/Volumes/MixedShare"
####################################################################
dNOW=`date +%F/%T`
DAY=`date +%w`
dDAY=`date +%d`
dMONTH=`date +%m`
dFULL=`date +%Y%m%d`
dtSTAMP=`date +%Y%m%d%H%M%S`
SOURCE="/Volumes/MixedShare/EmailProcessor"
INCOMING="$SOURCE/incoming"
INPROGRESS="$SOURCE/inProgress"
PROCESSED="$SOURCE/processed"
DONE="$SOURCE/done"
STATUS="$SOURCE/status"
LOGS="$SOURCE/logs"
LOGFILE="$LOGS/$dtSTAMP.log"
ERRORLOG="$LOGS/$dtSTAMP.error.log"
