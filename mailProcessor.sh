#! /bin/bash

####################################################################
####################################################################
#  Grep & process emails from files
#                                                                ###
####################################################################
SCRIPTNAME="Super Mail processor"
####################################################################
#WHERE="/Volumes/MixedShare" 
####################################################################
dNOW=`date +%F/%T`
DAY=`date +%w`
dDAY=`date +%d`
dMONTH=`date +%m`
dFULL=`date +%Y%m%d`
dtSTAMP=`date +%Y%m%d%H%M%S`
SOURCE="/Volumes/MixedShare/EmailProcessor"
INCOMING="$SOURCE/incoming"
INPROGRESS="$SOURCE/inProgress"
PROCESSED="$SOURCE/processed"
DONE="$SOURCE/done"
STATUS="$SOURCE/status"
LOGS="$SOURCE/logs"
LOGFILE="$LOGS/$dtSTAMP.log"
ERRORLOG="$LOGS/$dtSTAMP.error.log"
CLEANLIST="$SOURCE/scripts/cleanlist.rb"


startScript ()
{


	echo "__________________________ PROCESSING FILES"
	nbline=0
	for LGDIR in ${INCOMING}/*/
	do
		LGDIR=${LGDIR%*/}
		echo "_FOLDER : ${LGDIR##*/}"
		LGDIR=${LGDIR##*/}
		listFiles=`find "${INCOMING}/${LGDIR}" -type f`

		if [[ -z ${listFiles} ]]
			then
			echo "${LGDIR} : empty list"
		else
			#echo "files in ${LGDIR} : ${listFiles}"

			for file in ${listFiles};
			do
				filename=`basename ${file}`
				echo ">${filename}"
				mv "${file}" "${INPROGRESS}/${LGDIR}_${filename}"
				echo "${INPROGRESS}/${LGDIR}_${filename}" >> "$LOGS/${dtSTAMP}"_fullList.txt
				((nbline = ${nbline} + 1 ))
			done

		fi
	done

	echo "__________________________ <END> PROCESSING FILES  : ${nbline}"


if [[ ${nbline} -eq 0 ]]
	then
	echo "NO FILE TO PROCESS"
	exitNow
fi

echo "__________________________ EXTRACTING EMAILS"
totalEmails=0
for ((a=1; a<=$nbline; a++))
do
filename=`sed -n "$a"p "$LOGS/$dtSTAMP"_fullList.txt`
filenamestrict=`basename "${filename}"`
language=`echo "${filenamestrict}"|cut -f 1 -d "_"`
languageFile="${INPROGRESS}/${language}.csv"
echo ">> extracting emails from file :  ${filenamestrict}"
grep -R -E -o "\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b" "${filename}" | cut -d : -f 2 >> ${languageFile}
nbresults=`wc -l "${languageFile}" | awk {'print $1'}`
if  [ "$nbresults" -gt 0 ]
	then
	echo "DONE ${filenamestrict} : $nbresults emails"
else
	echo "DONE ${filenamestrict}: NO RESULT"
	#rm "${languageFile}"
fi
mv "${filename}" "${PROCESSED}/${dtSTAMP}_${filenamestrict}"
gzip "${PROCESSED}/${dtSTAMP}_${filenamestrict}"
((totalEmails = ${totalEmails} + ${nbresults} ))
done


echo "__________________________ <END> EXTRACTING EMAILS  : $totalEmails"

echo "__________________________ DEDUPLICATE EMAILS"
totalEmails=0
for f in ${INPROGRESS}/*
do
	echo "File : ${f}"
	filename=`basename "${f}"`
	cat "${f}" >> ${DONE}/${filename}
	rm ${f}
	awk '!a[$0]++' ${DONE}/${filename} > ${INPROGRESS}/${filename}_dedup
	nbresults=`wc -l "${INPROGRESS}/${filename}_dedup" | awk {'print $1'}`
	echo "${filename}_dedup : ${nbresults} emails"
	((totalEmails = ${totalEmails} + ${nbresults} ))
done
if [ ! -f ${INPROGRESS}/BLACK.csv_dedup ]
then
	echo "add BLACK_dedup to inProgress"
	cat "${DONE}/BLACK.csv" > ${INPROGRESS}/BLACK.csv_dedup
fi

echo "__________________________ END DEDUPLICATE EMAILS  : $totalEmails"

echo "__________________________ CLEANBLACKLIST EMAILS"
totalEmails=0
for f in ${INPROGRESS}/*
do
	filenames=`basename "${f}"`
	language=`echo "${filenames}"|cut -f 1 -d "."`
	if [ ${language} != "BLACK" ]
		then
		echo "removing BLACK from : ${language}"
		rm ${DONE}/${language}.csv
		ruby ${CLEANLIST} ${f} ${INPROGRESS}/BLACK.csv_dedup > ${DONE}/${language}.csv
		nbresults=`wc -l "${DONE}/${language}.csv" | awk {'print $1'}`
		rm ${f}
		echo "${language}.csv : ${nbresults} emails"
		((totalEmails = ${totalEmails} + ${nbresults} ))
	fi
done
echo "Cleaning up"
rm ${DONE}/BLACK.csv
mv ${INPROGRESS}/BLACK.csv_dedup ${DONE}/BLACK.csv
nbresults=`wc -l "${DONE}/BLACK.csv" | awk {'print $1'}`
echo "BLACK.csv : ${nbresults} emails"
(( totalEmails = ${totalEmails} + ${nbresults} ))
rm "$LOGS/${dtSTAMP}"_fullList.txt
find ${LOGS} -size  0 -print0 |xargs -0 rm
echo "__________________________ <END> CLEANBLACKLIST EMAILS  : $totalEmails"
}

############
############ NO NEED TO CHANGE FUNCTIONS BELOW
############

init_log () ## Redirect STDIN & STDERR
{
## do not use space for field separator (for loops in file names)
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

if [ -e "$ERRORLOG" ]
	then
	rm $ERRORLOG
fi
#exec > >(tee "$LOGFILE") 2>&1
#exec 3>&1 1>>${LOGFILE} 2>&1

exec &>$LOGFILE
exec 2>$ERRORLOG #redirect stderr to LOGFILE

}

header() ##HEADER
{
	echo "##########################################################"
	echo "SCRIPT : $SCRIPTNAME"
	echo "##########################################################"
	echo "===================================================="
	echo "    TIME START : $dNOW"
	echo "===================================================="
}

exec_step()  ##STEP
{
#Usage: exec_step ID
echo "====>   START STEP $1 of ${#STEP[*]}  [`date +%F/%T`]"
echo "== ${STEP[$1]} =="
echo "= #${STEP_COMMAND[$1]}"
eval ${STEP_COMMAND[$1]}
echo  "====<  END STEP $1 < [`date +%F/%T`]"
echo
}


exitNow() ##FOOTER
{
	echo
	echo "PROCESSED ${totalEmails} emails "
	echo "===================================================="
	echo "   TIME END  :  `date +%F/%T`"
	echo "  started on :   ${dNOW}"
	echo "===================================================="


	IFS=$SAVEIFS
	mv "$STATUS"/inProgress "$STATUS"/finished
	exit $EXITVAL
}



############
############ SCRIPT BODY
############

if  [ -e "$STATUS"/start ]
	then
	mv "$STATUS"/start "$STATUS"/inProgress
	init_log
	header
	startScript
	for ((a=1; a<=${#STEP[*]}; a++))
	do exec_step $a
done
exitNow
else
	exit
fi
