#!/bin/bash

### VARIABLES ### \
EMAIL="alain.p@interactiveoption.com"
SERVER=$(hostname)
MYSQLCREDS="-u root -pa14ld4iGYk"
ACCEPTABLEDELAY=500
MYSQL_CHECK=$(mysql ${MYSQLCREDS} -e "SHOW VARIABLES LIKE '%version%';" || echo 1)
LAST_ERRNO=$(/usr/bin/mysql ${MYSQLCREDS} -e "SHOW SLAVE STATUS\G" | grep "Last_Errno" | awk '{ print $2 }')
SECONDS_BEHIND_MASTER=$(/usr/bin/mysql ${MYSQLCREDS} -e "SHOW SLAVE STATUS\G"| grep "Seconds_Behind_Master" | awk '{ print $2 }')
IO_IS_RUNNING=$(/usr/bin/mysql ${MYSQLCREDS} -e "SHOW SLAVE STATUS\G" | grep "Slave_IO_Running" | awk '{ print $2 }')
SQL_IS_RUNNING=$(/usr/bin/mysql ${MYSQLCREDS} -e "SHOW SLAVE STATUS\G" | grep "Slave_SQL_Running" | awk '{ print $2 }')
ERRORS=()
NBERRORS=0

### Run Some Checks ###

## Check if I can connect to Mysql ##
if [ "$MYSQL_CHECK" == 1 ]
then
  ((NBERRORS++))
    ERRORS=("${ERRORS[@]}" "${NBERRORS}- CRITCAL > Can't connect to MySQL (Check Pass)")
fi

## Check For Last Error ##
if [ "$LAST_ERRNO" != 0 ]
then
  ((NBERRORS++))
    ERRORS=("${ERRORS[@]}" "${NBERRORS}-CRITCAL > Error when processing relay log (Last_Errno)")
fi

## Check if IO thread is running ##
if [ "$IO_IS_RUNNING" != "Yes" ]
then
  ((NBERRORS++))
    ERRORS=("${ERRORS[@]}" "${NBERRORS}-CRITCAL >I/O thread for reading the master's binary log is not running (Slave_IO_Running)")
fi

## Check for SQL thread ##
if [ "${SQL_IS_RUNNING:0:3}" != "Yes" ]
then
  ((NBERRORS++))
    ERRORS=("${ERRORS[@]}" "${NBERRORS}-CRITCAL > SQL thread for executing events in the relay log is not running (Slave_SQL_Running)")
fi

## Check how slow the slave is ##
if [ "$SECONDS_BEHIND_MASTER" == "NULL" ]
then
  ((NBERRORS++))
    ERRORS=("${ERRORS[@]}" "${NBERRORS}- The Slave is reporting 'NULL' (Seconds_Behind_Master)")
elif [ "$SECONDS_BEHIND_MASTER" -gt ${ACCEPTABLEDELAY} ]
then
  ((NBERRORS++))
    ERRORS=("${ERRORS[@]}" "${NBERRORS}-Non critical > The Slave is at $SECONDS_BEHIND_MASTER seconds behind the master (Seconds_Behind_Master)")
fi

echo -e "ERRORS : ${NBERRORS}"
### Send and Email if there is an error ###
if [ "${NBERRORS}" -gt 0 ]
then
    MESSAGE="An error has been detected on ${SERVER} involving the mysql replication. Below is a list of the reported errors:\n
    ---------------------------\n
    $(for i in $(seq 0 ${#ERRORS[@]}) ; do echo "\t${ERRORS[$i]}\n" ; done)
    ---------------------------\n
    IO_IS_RUNNING : $IO_IS_RUNNING\n
    SQL_IS_RUNNING : $SQL_IS_RUNNING\n
    "
    echo -e $MESSAGE #| mail -s "Mysql Replication for $SERVER is reporting Error" ${EMAIL}
fi
