var async = require("async");

async.series([
    function(callback){
        // do some stuff ...
        callback(null, one);
    },
    function(val1,callback){
        // do some more stuff ...
        console.log(val1);
        callback(null, val1);
        //callback(null, "three");
    }
],
// optional callback
function(err, results){
    console.log(results)
    // results is now equal to ['one', 'two']
});

function one(){
  console.log("Haha");
}
